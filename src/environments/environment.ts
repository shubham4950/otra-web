

export const environment = {
  production: false,

   base_url:'http://otra.co.in/api',

  //  base_url:'http://localhost:9000/api',

  firebase: {
    apiKey: "AIzaSyCzLcGB3MbfH7lfAXehQRFh6cjU3RkXhN4",
    authDomain: "otra-90da5.firebaseapp.com",
    databaseURL: "https://otra-90da5-default-rtdb.firebaseio.com",
    projectId: "otra-90da5",
    storageBucket: "otra-90da5.appspot.com",
    messagingSenderId: "432020783355",
    appId: "1:432020783355:web:63a6ae7f651e864981305e",
    measurementId: "G-WV4TD4RHK7"
  }
};
