import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommunicationService } from '../communication.service';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(


    private formBuilder: FormBuilder,
    private communication: CommunicationService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  submitted: boolean = false;
  personalSubmission: boolean = false;
  verifySubmission: boolean = false;

  forgotPassword: any = FormGroup;

  data: any;
  otp: any;
  msg: any;
  getData: any;
  getOtp: any;
  dataOtp: any;

  ngOnInit(): void {

    $('.Register_box2').hide();
    $('.Register_box3').hide();
    $('.Register_box4').hide();

    this.forgotPassword = this.formBuilder.group({
      username: ['', [Validators.required]],
      newpassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
      mobileNumber: ['', [Validators.required]],
      otp1: ['', [Validators.required]],
      otp2: ['', [Validators.required]],
      otp3: ['', [Validators.required]],
      otp4: '',
    });
  }
  anotherWay() {

    $('.Register_box1').hide();
    $('.Register_box2').show();
    $('.Register_box3').hide();
    $('.Register_box4').hide();
  }
  anotherWay2() {

    $('.Register_box1').show();
    $('.Register_box2').hide();
    $('.Register_box3').hide();
    $('.Register_box4').hide();
  }
  personal() {

    this.personalSubmission = true;
    if (this.forgotPassword.controls.username.status == 'INVALID' && this.forgotPassword.controls.mobileNumber.status == 'INVALID') {
      console.log(this.forgotPassword.value.username, "this.forgotPassword.value.username");
      alert('Please enter valid E-mail address or Mobile Number')
      return;
    }
    else {
      this.communication.verifyAccount(this.forgotPassword.value).subscribe(data => {
        console.log(data, "send Otp Api data");
        this.data = data
        this.dataOtp = this.data.data
        this.otp = this.dataOtp.otp
      })
      $('.Register_box1').hide();
      $('.Register_box3').show();
      $('.Register_box2').hide();
      $('.Register_box4').hide();
    }
  }

  sendOtp() {
    this.communication.sendOtp(this.forgotPassword.value).subscribe(data => {
      console.log(data, "send Otp Api data");
      this.data = data
      this.dataOtp = this.data.data
      this.otp = this.dataOtp.otp
    })
  }

  verifyOtp() {
    this.verifySubmission = true;
    if (this.forgotPassword.controls.otp1.status == 'INVALID' || this.forgotPassword.controls.otp2.status == 'INVALID' || this.forgotPassword.controls.otp3.status == 'INVALID') {
      alert('Please enter Otp for Verification')
      return;
    } else {
      this.getOtp = `${this.forgotPassword.value.otp1 + this.forgotPassword.value.otp2 + this.forgotPassword.value.otp3 + this.forgotPassword.value.otp4}`
      if (this.otp == this.getOtp) {
        $('.Register_box1').hide();
        $('.Register_box2').hide();
        $('.Register_box3').hide();
        $('.Register_box4').show();
      }
      else {
        alert('Incorrect Otp')
      }
    }
  }

  onClick() {
    console.log(this.forgotPassword)
    if (this.forgotPassword.controls.newpassword.status == 'INVALID' || this.forgotPassword.controls.confirmPassword.status == 'INVALID') {
      alert('Please enter new password & confirm password');
      return;
    }
    else {
      const pass = this.forgotPassword.controls.newpassword.value;
      const confirmPass = this.forgotPassword.controls.confirmPassword.value;

      if (pass == confirmPass) {
        this.submitted = true;
        if (this.submitted = true) {
          console.log(this.forgotPassword.value, "values")
          this.communication.recoverPassword(this.forgotPassword.value).subscribe(data => {
            console.log(data, "data forgotPassword2.value");
            this.router.navigate(['/login'], { relativeTo: this.route });
          })
          // if (this.forgotPassword.valid) {
          //   this.router.navigate(['/login'], { relativeTo: this.route });
          // }
        }
      }
      else {
        alert('Password missmatch ..please enter same password');
      }
    }
  }

  // submitPassword(){
  //   if(this.forgotPassword.valid){
  //     this.router.navigate(['/login'], {relativeTo: this.route});
  //   }
  // }

}
