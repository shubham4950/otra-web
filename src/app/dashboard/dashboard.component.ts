import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private router:Router,
       private route:ActivatedRoute,
       private jwtHelper: JwtHelperService) { }

  ngOnInit(): void {
    var token:any= localStorage.getItem('token')
    if (this.jwtHelper.isTokenExpired(token)) {
      localStorage.removeItem('token')
      this.router.navigate(['/login'], { relativeTo: this.route })
    }

    if(!token){
      localStorage.removeItem('token')
      this.router.navigate(['/login'], { relativeTo: this.route })
    }
  }

  searchCandidate(){
    this.router.navigate(['./searchCandidate'], { relativeTo: this.route })
  }

  support(){
    this.router.navigate(['./support'], { relativeTo: this.route })
  }
  subscription(){
    this.router.navigate(['./subscriptionPlan'], { relativeTo: this.route })

  }
  postedJobs(){
    this.router.navigate(['./jobsPosted'], { relativeTo: this.route })
    // window.location.reload();
  }
  getTeachers(){
    this.router.navigate(['./getTeachersList'], { relativeTo: this.route })
  }

  jobpost(){
    this.router.navigate(['../dashboard'], { relativeTo: this.route })
    // window.location.reload();
  }
  logOut(){
    //localStorage.removeItem('fireBaseToken');
    if(confirm('Do you want to logout ?')){
    localStorage.removeItem('user');
    localStorage.removeItem('token')
    this.router.navigate(['/login'], { relativeTo: this.route })
    }

  }

  
  navigationIcon: any = document.getElementById("sidebarIcon")
  sidebarContainer: any = document.getElementById("sidebarContainer")

  // navigationIcon.addEventListener("click", () => {
  //     console.log("hellow")
  //     sidebarContainer.classList.toggle("show")
  // })

}


