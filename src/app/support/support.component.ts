import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../communication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {


  customerSupport: any = FormGroup;
  constructor(
    private communication: CommunicationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
  ) { }
  id: any;
  userDetail:any;
  user:any;
  dataList:any;

  ngOnInit(): void {

    this.userDetail=localStorage.getItem('user');
    // this.user = JSON.parse(this.userDetail);
    // this.id = this.user._id
    // console.log(this.id, "school user_id");
    this.communication.customerQueryList({user_id:JSON.parse(this.userDetail)._id}).subscribe((data: any) => {
      console.log(data, "customerQueryList Data")
      this.dataList= data.data
      console.log( this.dataList, " this.dataList")
    })
    this.customerSupport = this.formBuilder.group({
      query: ''
    });
    
  }

  onSumbit() {
    if( this.customerSupport.value.query == ''){
      alert('Please enter your Description');
    }
    else{
    console.log(this.customerSupport.value, "this.customerSupport.value");
    this.customerSupport.value.user_id=JSON.parse(this.userDetail)._id;
    this.communication.customerSupport(this.customerSupport.value).subscribe(data => {
      console.log(data);
      // alert('send mail succesfully')
      // this.router.navigate(['../sendMail'], { relativeTo: this.activatedRoute })
    })
    this.userDetail=localStorage.getItem('user');
    // this.user = JSON.parse(this.userDetail);
    // this.id = this.user._id
    // console.log(this.id, "school user_id");
    this.communication.customerQueryList({user_id:JSON.parse(this.userDetail)._id}).subscribe((data: any) => {
      console.log(data, "customerQueryList Data")
      this.dataList= data.data
      console.log( this.dataList, " this.dataList")
    })
    this.customerSupport.reset();
  }
  }
}
