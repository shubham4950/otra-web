import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { CommunicationService } from '../communication.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-post-route-list',
  templateUrl: './post-route-list.component.html',
  styleUrls: ['./post-route-list.component.css']
})
export class PostRouteListComponent implements OnInit {


  constructor(
    private communication: CommunicationService,
    private cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {

  }

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  dataSource: any;
  displayedColumns: string[] = ['id', 'name', 'type','experience', 'icon'];
  getData: any;
  pdfSource:any;
  id:any;
  job_id:any;
  accessData:any
  username:any;
  job_status:any;
  jobStatus:any;


  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params.id;
    console.log(this.id,"line no 36");
    this.job_id = this.id;
    console.log(this.job_id,"job_id")

    

    this.communication.getPostRouteList({ job_id:this.id }).subscribe((data: any) => {
      console.log(data, "Eligible Candidate Data")
      this.dataSource = new MatTableDataSource(data['data']);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cdr.detectChanges();

      this.getData = data.data
   
    })


   }

  download(){
    window.open(`${this.accessData.resume}`);

  }
  userData(data:any){
    this.accessData = data;

    
  }
}
