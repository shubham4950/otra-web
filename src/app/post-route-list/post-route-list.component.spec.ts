import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostRouteListComponent } from './post-route-list.component';

describe('PostRouteListComponent', () => {
  let component: PostRouteListComponent;
  let fixture: ComponentFixture<PostRouteListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostRouteListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostRouteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
