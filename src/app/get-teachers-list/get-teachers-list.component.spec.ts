import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetTeachersListComponent } from './get-teachers-list.component';

describe('GetTeachersListComponent', () => {
  let component: GetTeachersListComponent;
  let fixture: ComponentFixture<GetTeachersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetTeachersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetTeachersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
