import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ChangeDetectorRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { CommunicationService } from '../communication.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-get-teachers-list',
  templateUrl: './get-teachers-list.component.html',
  styleUrls: ['./get-teachers-list.component.css']
})
export class GetTeachersListComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private communicationService: CommunicationService,
    private cdr: ChangeDetectorRef
  ) {
    this.dataSource.data = this.people;
    this.dataSource.filterPredicate = this.createFilter();
  }
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  columnsToDisplay = ['name', 'id', 'favouriteColour', 'pet', 'view'];
  nameFilter = new FormControl('');
  idFilter = new FormControl('');
  colourFilter = new FormControl('');
  petFilter = new FormControl('');
  dataSource = new MatTableDataSource();
  @Output() redirect: EventEmitter<any> = new EventEmitter();
  filterValues = {
    name: '',
    id: '',
    colour: '',
    pet: '',
  };
  people: any;
  accessData: any;
  images: any;
  imageData: any;
  resumeData: any;

  ngOnInit(): void {

    this.communicationService.getTeachersList({}).subscribe((data: any) => {
      console.log(data, "line no 33");
      this.people = data.data
      //this.dataSource = this.people;
      console.log(this.dataSource.data, "line no 35 +++++++++++++++++++++++++++++++++++++++++++++++");
      this.dataSource = new MatTableDataSource(data.data);
      // console.log(this.dataSource, "this.dataSiurce++++++++++++++++++++++")
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cdr.detectChanges();
    })

    this.nameFilter.valueChanges
      .subscribe(
        name => {
          this.filterValues.name = name;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.idFilter.valueChanges
      .subscribe(
        id => {
          this.filterValues.id = id;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.colourFilter.valueChanges
      .subscribe(
        favouriteColour => {
          this.filterValues.colour = favouriteColour;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.petFilter.valueChanges
      .subscribe(
        pet => {
          this.filterValues.pet = pet;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
  }

  applyFilter(filterValue: any) {
    console.log(filterValue)
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    filterValue = filterValue.toString();
    this.dataSource.filter = filterValue;
  }
  
  createFilter(): (data: any, filter: string) => boolean {
    let filterFunction = function (data: any, filter: any): boolean {
      let searchTerms = JSON.parse(filter);
      return data.username.toLowerCase().indexOf(searchTerms.name) !== -1
        && data.email.toString().toLowerCase().indexOf(searchTerms.id) !== -1
        && data.address.toLowerCase().indexOf(searchTerms.colour) !== -1
        && data.subject.toLowerCase().indexOf(searchTerms.pet) !== -1;
    }
    return filterFunction;
  }

  userData(data: any) {
    this.accessData = data;
    this.imageData = data.profileImage;

    this.images = `http://otra.co.in:9000/upload/${this.imageData}`
    this.resumeData = data.resume;
  }
  download() {
    window.open(`http://otra.co.in:9000/upload/${this.resumeData}`);

  }
}



