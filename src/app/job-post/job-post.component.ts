import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CommunicationService } from '../communication.service';
import { MapComponent } from '../map/map.component';

@Component({
  selector: 'app-job-post',
  templateUrl: './job-post.component.html',
  styleUrls: ['./job-post.component.css']
})
export class JobPostComponent implements OnInit {

  postJob: any = FormGroup;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private communication: CommunicationService
  ) { }
  skills: any
  experience: any
  jobType: any
  ngOnInit(): void {
    this.postJob = this.formBuilder.group({
      location: ['', Validators.required],
      salary: ['', Validators.required],
      jobType: ['', Validators.required],
      totalVacancy: ['', Validators.required],
      jobDescription: ['', Validators.required],
      experience: ['', Validators.required],
      profession: ['', Validators.required],
      skills: ['', Validators.required],
    });

    this.communication.getExperienceList().subscribe((data: any) => {
      this.experience = data.data;
    })
    this.communication.getschoolTypeList().subscribe((data: any) => {
      this.jobType = data.data;
    })
  }

  onSelectType(typeId: number) {
    console.log(typeId, "typeId");
    if (typeId) {
      let school_type = typeId
      this.communication.getSubjectList({ school_type }).subscribe((data: any) => {
        console.log(data, "adatahjfhfekfhbwelkfjweo;ifher;ih")
        this.skills = data['data'];
        console.log(this.skills);
      })
    }
  }

  skill: any;
  showAddress: any;
  latlng: any

  onSubmit() {
    console.log(this.postJob, "Line no 51")
    console.log(this.postJob.value.skills, "line no 54");

    var arr = this.postJob.value.skills;
    console.log(arr, "line no 55");
    if (arr.length > 3) {
      alert("Please select atmost 3 skills!");
    } else {
      if (this.postJob.controls.location.status == 'INVALID' || this.postJob.controls.jobType.status == 'INVALID' || this.postJob.controls.salary.status == 'INVALID' || this.postJob.controls.totalVacancy.status == 'INVALID' || this.postJob.controls.jobDescription.status == 'INVALID' || this.postJob.controls.experience.status == 'INVALID' || this.postJob.controls.profession.status == 'INVALID' || this.postJob.controls.totalVacancy.status == 'INVALID') {
        alert('Please fill out all required feilds!');
        return;
      }
      else {
        var getUserDetail: any = localStorage.getItem('user');
        this.postJob.value.schoolName = JSON.parse(getUserDetail)._id;
        this.postJob.value.lat_lng = this.latlng
        this.communication.jobPost(this.postJob.value).subscribe(data => {
          alert('Job posted succesfully done!')
          this.router.navigate(['../jobsPosted'], { relativeTo: this.route })
        })
      }
    }
  }

  openMap() {
    const dialogRef = this.dialog.open(MapComponent, {
      width: "580px"
    })
    dialogRef.afterClosed().subscribe((data: any) => {
      console.log(data, 'ghhfhgfgh');
      this.showAddress = data.address
      //  this.postJob.value.address=data.address;
      this.latlng = `${data.lat},${data.lng}`;
      // console.log(this.postJob.value.address_lat_long)
    })
  }
}
