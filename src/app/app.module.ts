import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderDashboardComponent } from './header-dashboard/header-dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VerifyOtpComponent } from './verify-otp/verify-otp.component';
import { CountryflagDirective } from './countryflag.directive';
import { AgmCoreModule } from '@agm/core';
import { MapComponent } from './map/map.component';
import { JobPostComponent } from './job-post/job-post.component';
import { SearchCandidateComponent } from './search-candidate/search-candidate.component';
import { SupportComponent } from './support/support.component';
import { PostedJobListingComponent } from './posted-job-listing/posted-job-listing.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { PostRouteListComponent } from './post-route-list/post-route-list.component';
import { GetCandidateDetailComponent } from './get-candidate-detail/get-candidate-detail.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { JwtModule } from "@auth0/angular-jwt";
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';

import { GetUserProfileComponent } from './get-user-profile/get-user-profile.component';
import { SubscriptionPlanComponent } from './subscription-plan/subscription-plan.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { GetTeachersListComponent } from './get-teachers-list/get-teachers-list.component';

import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { MessagingService } from './messaging.service';
import { environment } from '../environments/environment';
import { AsyncPipe } from '../../node_modules/@angular/common';

import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent},

  { path: 'map', component: MapComponent },
  {
    path: 'subscriptionPlan',
    component: SubscriptionPlanComponent
  },
  {
    path: 'dashboard', component: DashboardComponent,
    children: [
      {
        path: '',
        component: SearchCandidateComponent
      },
      {
        path: 'postJob',
        component: JobPostComponent
      },
      {
        path: 'getTeachersList',
        component: GetTeachersListComponent
      },
      {
        path: 'searchCandidate',
        component: SearchCandidateComponent
      },
      {
        path: 'support',
        component: SupportComponent
      },
      {
        path: 'jobsPosted',
        component: PostedJobListingComponent
      },
      {
        path: 'postRouteList/:id',
        component: PostRouteListComponent
      },
      {
        path: 'getCandidateDetail/:job_id/:user_id',
        component: GetCandidateDetailComponent
      },
      {
        path: 'sendMail',
        component: GetUserProfileComponent
      },
      {
        path: 'getUserProfile/:id',
        component: GetUserProfileComponent
      },
      {
        path: 'editDetail/:id',
        component: GetUserProfileComponent
      },
      {
        path: 'changePassword',
        component: GetUserProfileComponent
      },
      {
        path: 'subscriptionPlan',
        component: SubscriptionPlanComponent
      }
    ]
  },
  { path: 'dashboard/postJob', component: JobPostComponent },
  { path: 'dashboard/searchCandidate', component: SearchCandidateComponent },

]
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderDashboardComponent,
    LoginComponent,
    RegisterComponent,
    VerifyOtpComponent,
    CountryflagDirective,
    MapComponent,
    JobPostComponent,
    SearchCandidateComponent,
    SupportComponent,
    PostedJobListingComponent,
    PostRouteListComponent,
    GetCandidateDetailComponent,
    GetUserProfileComponent,
    SubscriptionPlanComponent,
    ForgotPasswordComponent,
    GetTeachersListComponent,

    PrivacyPolicyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule,
    MatDialogModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCsj7DME_8fg1rReVaoOqS87WBE3QjE67w',
      libraries: ['places']
    }),
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    PdfViewerModule,
    JwtModule,
    MatSelectModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ],
  providers: [{ provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,
    MessagingService, AsyncPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
