import { MapsAPILoader } from '@agm/core';
import { ChangeDetectorRef } from '@angular/core';
import { Component, ElementRef, Inject, NgZone, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { CommunicationService } from '../communication.service';
import { JobPostComponent } from '../job-post/job-post.component';
import { RegisterComponent } from '../register/register.component';
declare let google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  constructor(
    private apiloader: MapsAPILoader,
    private ngZone: NgZone,
    private communication:CommunicationService,
    public dialogRef: MatDialogRef<RegisterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cdr:ChangeDetectorRef
  ) { }
  title = 'My first AGM project';
  lat:any;
  lng:any;
  @ViewChild('search') searchElementRef: ElementRef;

  ngOnInit(): void {
  this.communication.getCurrentLocation().subscribe((data:any)=>{
    console.log(data,'data')
    this.lat=data.location.lat;
    this.lng=data.location.lng
    let latlng = {
      lat: data.location.lat,
      lng:data.location.lng
  };
    this.getLocationName(latlng);
    this.cdr.detectChanges();
  })

  }

  

  address:any
  getcurrentLocation:any
  public  onMapReady(map:any) {
    map.addListener('click',async (e:google.maps.MouseEvent) => {
      this.lat=e.latLng.lat();
      this.lng= e.latLng.lng();
      console.log(this.lat,this.lng)
      console.log(e.latLng.lat(), e.latLng.lng());
      let latlng = {
          lat: e.latLng.lat(),
          lng: e.latLng.lng()
      };
   
   await this.getLocationName(latlng);

    })
  
  }
  

 async getLocationName(latLng:any):Promise<Observable<any>>{
  let val=this;
  let geocoder = new google.maps.Geocoder;
  let location:any={}
  geocoder.geocode({
    'location': latLng
},async function(results:any,status:any) {
    if (results[0]) {
       console.log(results[0])

       val.address = results[0].formatted_address;
      location['currentLocation']=results[0].formatted_address
  
    } else {
      location['currentLocation']=null

        console.log('Not found',status);
       
    }
});

console.log(location,'location')
return  location
  }
  
  findAdress(){

  this.apiloader.load().then(() => {
    let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
    autocomplete.addListener("place_changed", () => {
      this.ngZone.run(() => {
        // some details
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
       //  this.address = place.formatted_address;
       //  this.web_site = place.website;
       //  this.name = place.name;
       //  this.zip_code = place.address_components[place.address_components.length - 1].long_name;
        //set latitude, longitude and zoom
        console.log(place)
        this.lat = place?.geometry?.location?.lat();
        this.lng = place?.geometry?.location?.lng();
        // this.zoom = 12;
        // this.getLocation(this.lat,this.lng)
      });
    });
  });
}
ngAfterViewInit(){
this.findAdress();
}
save(){
  var json={
   lat: this.lat,
    lng:this.lng,
    address:this.address
  }
  this.dialogRef.close(json);
}

close(){
  this.dialogRef.close();
}
}
 