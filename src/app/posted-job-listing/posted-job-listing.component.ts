import { JsonpClientBackend } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-posted-job-listing',
  templateUrl: './posted-job-listing.component.html',
  styleUrls: ['./posted-job-listing.component.css']
})
export class PostedJobListingComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private communicationService: CommunicationService,
    private cdr: ChangeDetectorRef
  ) { }
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  
  dataSource: any;
  jobData:any;
  data1:any;

  displayedColumns: string[] = ['id', 'name', 'date', 'job_id'];

  ngOnInit(): void {
    var getUser_id: any = localStorage.getItem('user');
    console.log(JSON.parse(getUser_id)._id, "line no 31");
    this.communicationService.jobPostList({ school_id: JSON.parse(getUser_id)._id }).subscribe((data: any) => {
      console.log(data, "line no 33");
      this.data1 = data.data.reverse();
      console.log(this.data1, "line no 39 +++++++++++++++++++++++++++++++++++++++++++++++")
      this.dataSource = new MatTableDataSource(data['data']);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cdr.detectChanges();
      
    })

  


  }
  applyFilter(filterValue: any) {
    console.log(filterValue)
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    filterValue = filterValue.toString();
    this.dataSource.filter = filterValue;
  }


  addJob() {
    this.router.navigate(['../postJob'], { relativeTo: this.route })

  }

  onSave(id: any) {
    console.log(id, "===========")
    this.router.navigate([`../postRouteList/${id}`], { relativeTo: this.route })
  }

  onClick(job_id:any){
    this.communicationService.viewJobDetail({job_id}).subscribe((data:any) => {
      console.log(data, "console data")
      this.jobData = data.data.job_detail;
      console.log(this.jobData, "data.job_detail")
    })
  }
}
