import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostedJobListingComponent } from './posted-job-listing.component';

describe('PostedJobListingComponent', () => {
  let component: PostedJobListingComponent;
  let fixture: ComponentFixture<PostedJobListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostedJobListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostedJobListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
