import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageServiceService {
private subject=new Subject();
  constructor() { 

  }

sendMesage(message:String){
  this.subject.next(message)
}
clearMessage(){
  this.subject.next()
}

getMessage():Observable<any>{
return this.subject.asObservable()
}
}
