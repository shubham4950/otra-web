import { JsonpClientBackend } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-header-dashboard',
  templateUrl: './header-dashboard.component.html',
  styleUrls: ['./header-dashboard.component.css']
})
export class HeaderDashboardComponent implements OnInit {

  constructor(private communication: CommunicationService,
    private router: Router,
    private route: ActivatedRoute) { }
  username: any;
  email: any;
  id: any;
  image: any;
  ngOnInit(): void {
    var user: any = localStorage.getItem('user');
    user = JSON.parse(user);
    this.username = user.username
    this.email = user.email
    this.id = user._id
    this.image = user.profileImage              //`http://192.168.1.110:9000/upload/${user.profileImage}`;
    console.log(this.image)
  }
  logOut() {

    if (confirm('Do you want to logout ?')) {

      // localStorage.removeItem('fireBaseToken');
      localStorage.removeItem('user');
      localStorage.removeItem('token')
      this.router.navigate(['/login'], { relativeTo: this.route })
    }
  }

  profile(id: any) {
    this.router.navigate([`./getUserProfile/${id}`], { relativeTo: this.route })


  }

  jobpost() {
    this.router.navigate(['../dashboard'], { relativeTo: this.route })
    // window.location.reload();
  }

  toggleSidebar() {
    document.getElementsByTagName('body')[0].classList.toggle("side_collapsed");
  }
  
dataList:any;
  onNotification() {
    const user_id = this.id
    console.log(user_id, "user_id")
    this.communication.getFcmNotificationList({user_id }).subscribe((data: any) => {
      console.log(data, "adatahjfhfekfhbwelkfjweo;ifher;ih")
       this.dataList = data['data'];
       console.log(this.dataList, "this.notiData");
    })
  }
}

