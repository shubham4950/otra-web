import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { CommunicationService } from '../communication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private communication:CommunicationService,
    private route: ActivatedRoute,
    private router:Router,

  ) { }
  login:any=FormGroup;
  ngOnInit(): void {
    this.login = this.formBuilder.group({
      email: ['',[Validators.required,Validators.email]],
      password: ['',Validators.required],

    
    });
  }
  onSubmit(){
    this.submitted = true;

    if (this.login.invalid) {
      // alert('invalid')
      return;
  }
  console.log(this.login.value,'login')
  this.login.value.role=2;
 this.login.value.device_token = localStorage.getItem('fireBaseToken');
 this.login.value.device_type = 'web'
;
  this.communication.login(this.login.value).subscribe((data:any)=>{
    console.log(data ,'login data');
     var response:any=data;
     if(response.status=='error'){
        alert(response.msg);
     }
     else{
    localStorage.setItem('token',data.data.token);
    localStorage.setItem('user',JSON.stringify(data.data.userDetail))

      this.router.navigate(['../dashboard'], { relativeTo: this.route })
     }
  })
    }
    get f() { return this.login.controls; }
    
  
   
}
