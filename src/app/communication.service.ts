import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  constructor(private http:HttpClient) { }

  login(json:any){
    return this.http.post(`${environment.base_url}/userLogin`,json);
  }
  register(json:any){
    var data=new FormData();
    data.append('address',json.address);
    data.append('address_lat_long',json.address_lat_long)
    data.append('password',json.password)
    data.append('country',json.country)
    data.append('state',json.state)
    data.append('city',json.city)
    data.append('email',json.email)
    data.append('file',json.file)
    data.append('fromHere',json.fromHere)
    data.append('phoneNumber',json.phoneNumber)
    data.append('role',json.role)
    data.append('username',json.username)
    data.append('device_type',json.device_type)
    data.append('device_token',json.device_token)

    return this.http.post(`${environment.base_url}/userRegister`,data);
  }

  hearedFrom(){
    return this.http.get(`${environment.base_url}/hearedFromList`);
  }
  sendOtp(json:any){
    return this.http.post(`${environment.base_url}/sendOtp`,json)
  }

  verifyAccount(json:any){
    return this.http.post(`${environment.base_url}/verifyAccount`,json)
  }

  getCurrentLocation(){
    return this.http.post(`https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyA0tL8trx2Oy4pq3vPUuOp1ZUMb3nhX6VI`,{})
  }

  jobPost(json:any){
   var token:any= localStorage.getItem('token')
  
    return this.http.post(`${environment.base_url}/jobPost`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
  })
  }
  
   jobPostList(json:any){
    var token:any= localStorage.getItem('token')
  
    return this.http.post(`${environment.base_url}/postList`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }

   getCandidateList(json:any){
    var token:any= localStorage.getItem('token')

     return this.http.post(`${environment.base_url}/candidateList`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }

   getUserDetail(json:any){
    var token:any= localStorage.getItem('token')

    return this.http.post(`${environment.base_url}/getUserDetail`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }

   getSubjectList(json:any){
    var token:any= localStorage.getItem('token')

    return this.http.post(`${environment.base_url}/subjectList`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getschoolTypeList(){
    var token:any= localStorage.getItem('token')

    return this.http.get(`${environment.base_url}/schoolTypeList`,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getPostRouteList(json:any){
    var token:any= localStorage.getItem('token')

    return this.http.post(`${environment.base_url}/candidateListEligible`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getCandidateDetail(json:any){
    var token:any= localStorage.getItem('token')

    return this.http.post(`${environment.base_url}/view_job_applied_by_user`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getExperienceList(){
    var token:any= localStorage.getItem('token')
    return this.http.get(`${environment.base_url}/experienceList`,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }

   getUserProfile(json:any){
    var token:any= localStorage.getItem('token')
    return this.http.post(`${environment.base_url}/getUserDetail`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   editDetail(json:any){
    var data=new FormData();
    data.append('id',json.id);
    data.append('profile',json.profile);
    data.append('username',json.username)
    data.append('email',json.email);
    data.append('phoneNumber',json.phoneNumber)
    data.append('address',json.address)  
    data.append('country',json.country)
    data.append('state',json.state)
    data.append('city',json.city)
    var token:any= localStorage.getItem('token')
    var token:any= localStorage.getItem('token')
     return this.http.post(`${environment.base_url}/updateProfile`,data,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
   })
   }
   changePassword(json:any){
    var token:any= localStorage.getItem('token')
     return this.http.post(`${environment.base_url}/forgotPassword`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
   })
   } 
    sendMail(json:any){
    var token:any= localStorage.getItem('token')
     return this.http.post(`${environment.base_url}/sendMessage`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
   })
   }
   subscriptionPlan(json:any){
    var token:any= localStorage.getItem('token')
    return this.http.get(`${environment.base_url}/planList`,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
  })
   }
   customerSupport(json:any){
    var token:any= localStorage.getItem('token')
     return this.http.post(`${environment.base_url}/addQuery`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
   })
   }
   customerQueryList(json:any){
    var token:any= localStorage.getItem('token')
     return this.http.post(`${environment.base_url}/query_list_user`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
   })
   }
   acceptForJob(json:any){
    var token:any= localStorage.getItem('token')
     return this.http.post(`${environment.base_url}/update_applied_job_status`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
   })
   }
   viewMail(json:any){
    var token:any= localStorage.getItem('token')
     return this.http.post(`${environment.base_url}/view_mail`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})
   })
   }
   viewJobDetail(json:any){
    var token:any= localStorage.getItem('token')
    return this.http.post(`${environment.base_url}/job_detail`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   recoverPassword(json:any){
    var token:any= localStorage.getItem('token')
    return this.http.post(`${environment.base_url}/forgotPassword`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getTeachersList(json:any){
    var token:any= localStorage.getItem('token')

     return this.http.post(`${environment.base_url}/teacherList`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getCountryList(){
    var token:any= localStorage.getItem('token')

    return this.http.get(`${environment.base_url}/countryList`,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getStateList(json:any){
    var token:any= localStorage.getItem('token')

     return this.http.post(`${environment.base_url}/stateList`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getCityList(json:any){
    var token:any= localStorage.getItem('token')

     return this.http.post(`${environment.base_url}/cityList`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
   getFcmNotificationList(json:any){
    var token:any= localStorage.getItem('token')

     return this.http.post(`${environment.base_url}/fcmList`,json,{ headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
   }
}
