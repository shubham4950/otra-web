import { Component, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { CommunicationService } from '../communication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MessageServiceService } from '../message-service.service';
import { HttpHeaders } from '@angular/common/http';




@Component({
  selector: 'app-get-candidate-detail',
  templateUrl: './get-candidate-detail.component.html',
  styleUrls: ['./get-candidate-detail.component.css']
})
export class GetCandidateDetailComponent implements OnInit {
  sendMail: any = FormGroup;

  constructor(
    
    private communication: CommunicationService,
    private renderer: Renderer2,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageServiceService
  ) { }
  display: any
  dataSource: any;
  pdfSource: any;
  userDetail: any;
  id: any;
  job_id: any;
  user_id: any;
  sharedData: any
  status: any;
  jsonDataFromService: any;
  appliedData: any;
  school_id: any
  job_status: any;
  application_id: any;
  applicant_id: any
  mailData:any;
  accessData:any;
  images:any;
  submitted:boolean= false;
  getSendMail:any;
  msg:any;



  ngOnInit(): void {
    this.userDetail = localStorage.getItem('user');
   
    this.user_id = this.activatedRoute.snapshot.paramMap.get('user_id');
    console.log(this.user_id, "userid");
    this.applicant_id = this.user_id;

    this.job_id = this.activatedRoute.snapshot.paramMap.get('job_id');
    console.log(this.job_id, "jobid");

    this.school_id = JSON.parse(this.userDetail)._id
    var sharedData: any = {
      job_id: this.job_id, user_id: this.user_id
    }


    this.communication.getCandidateDetail(sharedData).subscribe((data: any) => {
      this.dataSource = data['data'][0];
      console.log(this.dataSource.status , "getcandidate Detail line no 70")
      this.pdfSource = `http://otra.co.in:9000/upload/${this.dataSource.teacher_resume}`;
      console.log( this.pdfSource, "pdfSource" );
      this.images = `http://otra.co.in:9000/upload/${this.dataSource.teacher_profile}`;
      console.log(this.images,"this.images");
      this.application_id = this.dataSource._id;
      console.log(this.application_id, "this.application_id")
      this.status = this.dataSource.status;
      console.log(this.status, "this.status");
    })



    this.sendMail = this.formBuilder.group({
      title: '',
      text: '',
    });

  


  }

  // ngAfterViewInit() {
  //   console.log('here data')
  //   this.messageService.getMessage().subscribe(dataget => {
  //     this.jsonDataFromService = dataget;
  //   })
  // }

  onSumbit(job_id: any, applicant_id: any, school_id: any, application_id: any) {
    //console.log(this.sendMail.value, "this.sendmail.value");
    this.sendMail.value.school_id = this.school_id;
    this.sendMail.value.user_id = this.user_id;
    this.sendMail.value.application_id = this.application_id;

    console.log(this.user_id, this.sendMail.value.job_id, "line no 95")
    this.job_status = 5;
    var job_status = this.job_status;
    this.communication.sendMail(this.sendMail.value).subscribe(data => {
      console.log(data);
      this.getSendMail = data;
      console.log(this.getSendMail, "this.getSendMail");
      console.log(this.getSendMail.status == "success", "swfghhhn");
      console.log(this.getSendMail.status);

this.submitted= true;
  if(this.getSendMail.status == "success"){
    this.msg= "Your mail has been sent successfully."
  }
  else{
    this.msg= "Your mail has been sent successfully."
  }

      this.communication.acceptForJob({ job_id, applicant_id, school_id, application_id, job_status }).subscribe(data => {
        console.log(data, "accept for job");
        var sharedData: any = {
          job_id: this.job_id, user_id: this.applicant_id
        }
        this.communication.getCandidateDetail(sharedData).subscribe((data: any) => {
          console.log(data, "data");
          this.dataSource = data['data'][0];
          this.pdfSource = this.dataSource.resume;
          this.application_id = this.dataSource._id;
          console.log(this.application_id, "this.application_id")
          this.status = this.dataSource.status;
          console.log(this.status, "this.status");
        })
      })
    })
  }
  onAccept(job_id: any, applicant_id: any, school_id: any, application_id: any) {
    this.job_status = 4;
    var job_status = this.job_status;
    console.log("working Accept")
    this.communication.acceptForJob({ job_id, applicant_id, school_id, application_id, job_status }).subscribe(data => {
      console.log(data, "accept for job");
      var sharedData: any = {
        job_id: this.job_id, user_id: this.applicant_id
      }
      this.communication.getCandidateDetail(sharedData).subscribe((data: any) => {
        console.log(data, "data candidate Detail line no 150");
        this.dataSource = data['data'][0];
        this.pdfSource = this.dataSource.resume;
        this.application_id = this.dataSource._id;
        console.log(this.application_id, "this.application_id")
        this.status = this.dataSource.status;
        console.log(this.status, "this.status");
      })
    })
  }

  onReject(job_id: any, applicant_id: any, school_id: any, application_id: any) {
    this.job_status = 3;
    var job_status = this.job_status;
    this.communication.acceptForJob({ job_id, applicant_id, school_id, application_id, job_status }).subscribe(data => {
      console.log(data);
      var sharedData: any = {
        job_id: this.job_id, user_id: this.applicant_id
      }
      this.communication.getCandidateDetail(sharedData).subscribe((data: any) => {
        console.log(data, "data");
        this.dataSource = data['data'][0];
        this.pdfSource = this.dataSource.resume;
        this.application_id = this.dataSource._id;
        console.log(this.application_id, "this.application_id")
        this.status = this.dataSource.status;
        console.log(this.status, "this.status");
      })

    })
  }
  viewMail(job_id: any, applicant_id: any, school_id: any, application_id: any) {
    this.job_status = 5;
    var job_status = this.job_status;
      this.communication.acceptForJob({job_id, applicant_id, school_id, application_id,  job_status }).subscribe(data=>{
        console.log(data, "accept for job" );
        var sharedData:any={
          job_id: this.job_id, user_id: this.applicant_id
        }
        this.communication.getCandidateDetail(sharedData).subscribe((data:any)=>{
          console.log(data , "data");
          this.dataSource= data['data'][0];
          this.pdfSource = this.dataSource.resume;
          this.application_id = this.dataSource._id;
          console.log(this.application_id,"this.application_id")
          this.status = this.dataSource.status;
          console.log(this.status,"this.status");
        })
      })
    this.communication.viewMail({application_id}).subscribe((data: any) => {
      console.log(data);
      this.mailData = data.data
      console.log(this.mailData, "mailData"); 
      this.accessData = this.mailData[0]
      console.log(this.accessData, "accessData")
    })
  }

  download(){
    window.open(`http://otra.co.in:9000/upload/${this.dataSource.teacher_resume}`);

  }
}
