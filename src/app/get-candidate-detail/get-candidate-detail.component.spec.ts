import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCandidateDetailComponent } from './get-candidate-detail.component';

describe('GetCandidateDetailComponent', () => {
  let component: GetCandidateDetailComponent;
  let fixture: ComponentFixture<GetCandidateDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetCandidateDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCandidateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
