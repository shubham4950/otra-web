import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../communication.service';
import { PipeTransform, Pipe } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Pipe({ name: 'keyValueUnsorted', pure: false })
export class KeyValuePipe implements PipeTransform {
  transform(input: any): any {
    let keys = [];
    for (let key in input) {
      if (input.hasOwnProperty(key)) {
        keys.push({ key: key, value: input[key] });
      }
    }
    return keys;
  }
}
@Component({
  selector: 'app-subscription-plan',
  templateUrl: './subscription-plan.component.html',
  styleUrls: ['./subscription-plan.component.css']
})

export class SubscriptionPlanComponent implements OnInit {
  constructor(
    private communication: CommunicationService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }


  plans: any;
  
  ngOnInit(): void {
    this.communication.subscriptionPlan({}).subscribe((data: any) => {
      console.log(data, "Subscription plan data");
      this.plans = data.data;
      console.log(this.plans, "this.plan")
    })
  }

  onChoosePlan() {
    this.router.navigate(['../login'], { relativeTo: this.route })
  }

}


