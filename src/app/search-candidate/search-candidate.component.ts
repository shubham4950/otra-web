import { Component, OnInit, Output, ViewChild,EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CommunicationService } from '../communication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageServiceService } from '../message-service.service';

@Component({
  selector: 'app-search-candidate',
  templateUrl: './search-candidate.component.html',
  styleUrls: ['./search-candidate.component.css']
})
export class SearchCandidateComponent implements OnInit {
school:any;


  @ViewChild(MatPaginator,{static:false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static:false}) sort: MatSort;
  people:any; 

  nameFilter = new FormControl('');
  idFilter = new FormControl('');
  colourFilter = new FormControl('');
  petFilter = new FormControl('');
  dataSource = new MatTableDataSource();
  jobFilter=new FormControl();
  columnsToDisplay = ['name', 'id', 'favouriteColour', 'pet','job'];
  @Output() redirect:EventEmitter<any> = new EventEmitter();
  filterValues = {
    name: '',
    id: '',
    colour: '',
    pet: '',
    jobFilter:''
  };

  constructor(
    private communicationService:CommunicationService,
    private router:Router,
    private route:ActivatedRoute,
    private messageService:MessageServiceService
    ) {
    this.dataSource.data = this.people;
    this.dataSource.filterPredicate = this.createFilter();
  }
  ngOnInit() {
    let schoolId:any=localStorage.getItem('user');
   let school_id= JSON.parse(schoolId)._id;
   this.school=JSON.parse(schoolId).username
    this.communicationService.getCandidateList({school_id}).subscribe(data=>{
      this.people=data
      this.dataSource.data = this.people;
     // console.log(this.people, "this.people.id");
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      console.log(this.people)
    })
   
    this.nameFilter.valueChanges
      .subscribe(
        name => {
          this.filterValues.name = name;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
      this.jobFilter.valueChanges
      .subscribe(
        job => {
          this.filterValues.jobFilter = job;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.idFilter.valueChanges
      .subscribe(
        id => {
          this.filterValues.id = id;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.colourFilter.valueChanges
      .subscribe(
        favouriteColour => {
          this.filterValues.colour = favouriteColour;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.petFilter.valueChanges
      .subscribe(
        pet => {
          this.filterValues.pet = pet;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
  }

  createFilter(): (data: any, filter: string) => boolean {
    let filterFunction = function(data:any, filter:any): boolean {
      let searchTerms = JSON.parse(filter);
      console.log(searchTerms)
      return data.candidate_name.toLowerCase().indexOf(searchTerms.name) !== -1
        && data.candidate_email.toString().toLowerCase().indexOf(searchTerms.id) !== -1
        && data.subject.toString().toLowerCase().indexOf(searchTerms.pet) !== -1
        && data.job_title.toLowerCase().indexOf(searchTerms.jobFilter) !== -1
        && data.candidate_address.toLowerCase().indexOf(searchTerms.colour) !== -1;
    }
    return filterFunction;
  }

  onSave(job_id:any,user_id:any){
    console.log(job_id,user_id,"job_id,user_id");
    // var sharedData:any={
    //   job_id,user_id
    // }
    // this.messageService.sendMesage(sharedData)
    this.router.navigate([`./getCandidateDetail/${job_id}/${user_id}`], { relativeTo: this.route })
  }
}
