import { Component, OnInit } from '@angular/core';
import { MessagingService } from './messaging.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'otrawebschool';
 message:any;
  constructor(private router: Router,
    private messagingService: MessagingService) { }

  ngOnInit() {
     console.log(this.router)
      this.messagingService.requestPermission()
     // this.messagingService.receiveMessage()
     this.message = this.messagingService.currentMessage
     console.log(this.message);
   }
 
   ngOnChanges() {
     console.log(this.router)
     // this.messagingService.requestPermission()
 
   } 
}
