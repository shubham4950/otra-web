importScripts('https://www.gstatic.com/firebasejs/8.4.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.4.1/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyCzLcGB3MbfH7lfAXehQRFh6cjU3RkXhN4",
    authDomain: "otra-90da5.firebaseapp.com",
    databaseURL: "https://otra-90da5-default-rtdb.firebaseio.com",
    projectId: "otra-90da5",
    storageBucket: "otra-90da5.appspot.com",
    messagingSenderId: "432020783355",
    appId: "1:432020783355:web:63a6ae7f651e864981305e",
    measurementId: "G-WV4TD4RHK7"
});

// const messaging = firebase.messaging();
 const messaging = firebase.messaging.isSupported() ? firebase.messaging() : null

